﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;

namespace Apagar_cs
{
    public partial class Principal : Form
    {
        string comando;
        public Principal(){
            InitializeComponent();
        }

       
        private void Form1_Load(object sender, EventArgs e) {
            //System.Diagnostics.Process.Start("cmd.exe", comando);
        }

        private void btnProgramar_Click(object sender, EventArgs e) {
            string  horas, minutos, segundos; //Variables para guardar los datos ingresados en texto
            int horasInt, minInt, segInt; //Variables para parsar de texto a entero
            int horasSegundos, minutosSegundos; //Variables para guardar la conversion
            int segundosTotales; //Suma todos los segundos

            //Obtiene, convierte y asigna valores introducidos por el usuario
            horas = tbHoras.Text.Trim().Replace(" ", string.Empty);
            minutos = tbMinutos.Text.Trim().Replace(" ", string.Empty);
            segundos = tbSegundos.Text.Trim().Replace(" ", string.Empty);

            //Comprueba si el campo esta vacio, si es verdad, asigna valor cero
            if (horas == "") horas = "0";
            if (minutos == "") minutos = "0";
            if (segundos == "") segundos = "0";


            try {
                //Intenta convertir lo ingresado a entero
                horasInt = Int32.Parse(horas);
                minInt = Int32.Parse(minutos);
                segInt = Int32.Parse(segundos);
            } catch (Exception) {
                //En caso de que lo anterior no se cumpla, muestra un msj y termina el proceso
                //Asigna valor vacio a los textbox
                MessageBox.Show("No se pueden agregar:\n" +
                    "    -Letras\n" +
                    "    -Numeros con punto decimal\n" +
                    "    -Caracteres especiales\n" +
                    "\nLo siento :(", "Error" ,MessageBoxButtons.OK , MessageBoxIcon.Error);
                limpiar();
                return;
            }
            


            //Convierte horas y minutos a segundos
            horasSegundos = datosAsegundos(datosAsegundos(horasInt));
            minutosSegundos = datosAsegundos(minInt);

            //Suma todos los segundos
            segundosTotales = horasSegundos + minutosSegundos + segInt;

            //Comprueba si los segundosTotales son igual a cero
            //Si es verdadero, enviará un msj avisando que el equipo se apagará inmediatamnte
            //Al dar clic en 'no', No se programará el apagado
            if (segundosTotales == 0) {
                if (MessageBox.Show("Al no establecer tiempo de apagado; el equipo se apagará inmediatamente :)", "Informacion", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    MessageBox.Show("El equipo se apagará ahora :)", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                    return;
            }

            //Crear cadena del comando
            comando = "shutdown /s /t " + segundosTotales;

            //Abre la consola en segundo plano y ejecuta el comando
            cmdExe(comando);
            limpiar();

        }

        private void btnCancelar_Click(object sender, EventArgs e){
            comando = "shutdown /a";
            cmdExe(comando);
            limpiar();
        }

        //Convertir datos a segundos
        int datosAsegundos(int dato){
            int resultado = dato * 60;
            return resultado;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e){
            //Al cerrar el Form, se libearan recursos utilizados
            Dispose();
        }

        //Funcion para abrir el proceso de cmd
        void cmdExe(string comando) {
            ProcessStartInfo cmd = new ProcessStartInfo("cmd", @"/c" + comando);
            cmd.RedirectStandardOutput = true;
            cmd.UseShellExecute = false;
            Process proc = new Process();
            proc.StartInfo = cmd;
            proc.Start();
        }

        void limpiar() {
            tbHoras.Text = "";
            tbMinutos.Text = "";
            tbSegundos.Text = "";
        }

        private void lkAcercaDe_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            acercaDe frm = new acercaDe();
            frm.Show();
        }
    }
}
