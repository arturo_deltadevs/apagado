﻿namespace Apagar_cs
{
    partial class Principal
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Principal));
            this.lbInstrucciones = new System.Windows.Forms.Label();
            this.lbHoras = new System.Windows.Forms.Label();
            this.lbMinutos = new System.Windows.Forms.Label();
            this.lbSegundos = new System.Windows.Forms.Label();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnProgramar = new System.Windows.Forms.Button();
            this.tbHoras = new System.Windows.Forms.TextBox();
            this.tbMinutos = new System.Windows.Forms.TextBox();
            this.tbSegundos = new System.Windows.Forms.TextBox();
            this.lbNoNecesario = new System.Windows.Forms.Label();
            this.lbTwitter = new System.Windows.Forms.Label();
            this.lkAcercaDe = new System.Windows.Forms.LinkLabel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lbInstrucciones
            // 
            this.lbInstrucciones.AutoSize = true;
            this.lbInstrucciones.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbInstrucciones.Location = new System.Drawing.Point(46, 9);
            this.lbInstrucciones.Name = "lbInstrucciones";
            this.lbInstrucciones.Size = new System.Drawing.Size(344, 16);
            this.lbInstrucciones.TabIndex = 4;
            this.lbInstrucciones.Text = "¿En cuánto tiempo quieres que se apague tu pc?";
            this.lbInstrucciones.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbHoras
            // 
            this.lbHoras.AutoSize = true;
            this.lbHoras.Location = new System.Drawing.Point(46, 77);
            this.lbHoras.Name = "lbHoras";
            this.lbHoras.Size = new System.Drawing.Size(35, 13);
            this.lbHoras.TabIndex = 5;
            this.lbHoras.Text = "Horas";
            // 
            // lbMinutos
            // 
            this.lbMinutos.AutoSize = true;
            this.lbMinutos.Location = new System.Drawing.Point(188, 77);
            this.lbMinutos.Name = "lbMinutos";
            this.lbMinutos.Size = new System.Drawing.Size(44, 13);
            this.lbMinutos.TabIndex = 6;
            this.lbMinutos.Text = "Minutos";
            // 
            // lbSegundos
            // 
            this.lbSegundos.AutoSize = true;
            this.lbSegundos.Location = new System.Drawing.Point(332, 77);
            this.lbSegundos.Name = "lbSegundos";
            this.lbSegundos.Size = new System.Drawing.Size(55, 13);
            this.lbSegundos.TabIndex = 7;
            this.lbSegundos.Text = "Segundos";
            // 
            // btnCancelar
            // 
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnCancelar.Location = new System.Drawing.Point(307, 134);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(118, 23);
            this.btnCancelar.TabIndex = 8;
            this.btnCancelar.Text = "Cancelar apagado";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnProgramar
            // 
            this.btnProgramar.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnProgramar.Location = new System.Drawing.Point(12, 134);
            this.btnProgramar.Name = "btnProgramar";
            this.btnProgramar.Size = new System.Drawing.Size(118, 23);
            this.btnProgramar.TabIndex = 0;
            this.btnProgramar.Text = "Programar apagado";
            this.btnProgramar.UseVisualStyleBackColor = true;
            this.btnProgramar.Click += new System.EventHandler(this.btnProgramar_Click);
            // 
            // tbHoras
            // 
            this.tbHoras.Location = new System.Drawing.Point(12, 93);
            this.tbHoras.Name = "tbHoras";
            this.tbHoras.Size = new System.Drawing.Size(100, 20);
            this.tbHoras.TabIndex = 9;
            this.tbHoras.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbMinutos
            // 
            this.tbMinutos.Location = new System.Drawing.Point(157, 93);
            this.tbMinutos.Name = "tbMinutos";
            this.tbMinutos.Size = new System.Drawing.Size(100, 20);
            this.tbMinutos.TabIndex = 10;
            this.tbMinutos.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbSegundos
            // 
            this.tbSegundos.Location = new System.Drawing.Point(307, 93);
            this.tbSegundos.Name = "tbSegundos";
            this.tbSegundos.Size = new System.Drawing.Size(100, 20);
            this.tbSegundos.TabIndex = 11;
            this.tbSegundos.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lbNoNecesario
            // 
            this.lbNoNecesario.AutoSize = true;
            this.lbNoNecesario.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNoNecesario.Location = new System.Drawing.Point(130, 40);
            this.lbNoNecesario.Name = "lbNoNecesario";
            this.lbNoNecesario.Size = new System.Drawing.Size(169, 12);
            this.lbNoNecesario.TabIndex = 13;
            this.lbNoNecesario.Text = "No es necesario llenar todos los campos";
            // 
            // lbTwitter
            // 
            this.lbTwitter.AutoSize = true;
            this.lbTwitter.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTwitter.Location = new System.Drawing.Point(0, 173);
            this.lbTwitter.Name = "lbTwitter";
            this.lbTwitter.Size = new System.Drawing.Size(57, 12);
            this.lbTwitter.TabIndex = 14;
            this.lbTwitter.Text = "@artikunazo";
            // 
            // lkAcercaDe
            // 
            this.lkAcercaDe.AutoSize = true;
            this.lkAcercaDe.Location = new System.Drawing.Point(176, 173);
            this.lkAcercaDe.Name = "lkAcercaDe";
            this.lkAcercaDe.Size = new System.Drawing.Size(56, 13);
            this.lkAcercaDe.TabIndex = 15;
            this.lkAcercaDe.TabStop = true;
            this.lkAcercaDe.Text = "Acerca de";
            this.lkAcercaDe.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lkAcercaDe_LinkClicked);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Apagar_cs.Properties.Resources.logo;
            this.pictureBox1.Location = new System.Drawing.Point(305, 166);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(135, 24);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 12;
            this.pictureBox1.TabStop = false;
            // 
            // Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
            this.ClientSize = new System.Drawing.Size(442, 186);
            this.Controls.Add(this.lkAcercaDe);
            this.Controls.Add(this.lbTwitter);
            this.Controls.Add(this.lbNoNecesario);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.tbSegundos);
            this.Controls.Add(this.tbMinutos);
            this.Controls.Add(this.tbHoras);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.lbSegundos);
            this.Controls.Add(this.lbMinutos);
            this.Controls.Add(this.lbHoras);
            this.Controls.Add(this.lbInstrucciones);
            this.Controls.Add(this.btnProgramar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Principal";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Apagar";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lbInstrucciones;
        private System.Windows.Forms.Label lbHoras;
        private System.Windows.Forms.Label lbMinutos;
        private System.Windows.Forms.Label lbSegundos;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnProgramar;
        private System.Windows.Forms.TextBox tbHoras;
        private System.Windows.Forms.TextBox tbMinutos;
        private System.Windows.Forms.TextBox tbSegundos;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lbNoNecesario;
        private System.Windows.Forms.Label lbTwitter;
        private System.Windows.Forms.LinkLabel lkAcercaDe;
    }
}

